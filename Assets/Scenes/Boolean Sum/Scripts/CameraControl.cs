﻿using System.Collections;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    private readonly float cameraSpeed = 0.7f; //Later update
    private Vector3 newPosition;
    public bool startPlay = false; //Start playing the game
    public GameManager gm; // For time fxn
    public ObjectSpawnerScript obs;

    private void Start()
    {
        newPosition = transform.position;
    }

    void Update()
    {
        //transform.position = new Vector3(transform.position.x, player.position.y + distanceBtwn, transform.position.z);
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            startPlay = true;
        }

        if (startPlay)
        {
            newPosition.y = transform.position.y + Time.deltaTime * cameraSpeed; //Updating position
            transform.position = newPosition;
        }

        StartCoroutine("DeleteBoard");

        if (gm.timeRemaining <= 0)
            startPlay = false;
      
    }

    //Thought in progress
    IEnumerator DeleteBoard()
    {
        yield return new  WaitForSeconds(60f);
        if (transform.position.y >= 15f)
            obs.DeleteSpawns();
    }

}
