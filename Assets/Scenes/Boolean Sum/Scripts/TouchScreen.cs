﻿using UnityEngine;


public class TouchScreen : MonoBehaviour
{
    public GameObject player;
    private Vector2 firstTouch;
    public int swipeDistance = 20;
    private bool fingerPress;

    private void Update()
    {
        if (fingerPress && Input.GetMouseButtonDown(0) /* == false && Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began*/)
        {
            firstTouch = Input.mousePosition;
            // firstTouch = Input.touches[0].position;
            fingerPress = true;
        }

        if (fingerPress/* && Input.GetMouseButtonUp(0)*/)
        {
            if (/*Input.touches[0].position.y >= firstTouch.y +*/ Input.mousePosition.y >= firstTouch.y + swipeDistance)
            {
                Debug.Log("Swipe up");
                fingerPress = false;
                //A swipe up
            }
        }
    }
}

//public class TouchScreen : MonoBehaviour
//{
//    private Vector2 fp;   //First touch position. It is a 2D game so need for vector 3
//    private Vector2 lp;   //Last touch position
//    private float dragDistance;  //minimum distance for a swipe to be registered

//    void Start()
//    {
//        dragDistance = Screen.height * 15 / 100; //dragDistance is 15% height of the screen
//    }

//    // Update is called once per frame
//    void Update()
//    {
//        if (Input.GetMouseButtonDown(0) && Input.touchCount == 1) // user is touching the screen with a single touch
//        {
//            Touch touch = Input.GetTouch(0); // get the touch
//            if (touch.phase == TouchPhase.Began) //check for the first touch
//            {
//                fp = touch.position;
//                lp = touch.position;
//            }
//            else if (touch.phase == TouchPhase.Moved) // update the last position based on where they moved
//            {
//                lp = touch.position;
//            }
//            else if (touch.phase == TouchPhase.Ended) //check if the finger is removed from the screen
//            {
//                lp = touch.position;  //last touch position.

//                //Check if drag distance is greater than 20% of the screen height
//                if (Mathf.Abs(lp.x - fp.x) > dragDistance || Mathf.Abs(lp.y - fp.y) > dragDistance)
//                {
//                    //It's a drag
//                    //check if the drag is vertical or horizontal

//                    if (Mathf.Abs(lp.x - fp.x) > Mathf.Abs(lp.y - fp.y))
//                    {   //If the horizontal movement is greater than the vertical movement...

//                        if ((lp.x > fp.x))  //If the movement was to the right)
//                        {   //Right swipe
//                            Debug.Log("Right Swipe");
//                        }
//                        else
//                        {   //Left swipe
//                            Debug.Log("Left Swipe");
//                        }
//                    }
//                    else
//                    {   //the vertical movement is greater than the horizontal movement
//                        if (lp.y > fp.y)  //If the movement was up
//                        {   //Up swipe
//                            // rb.velocity += Vector2.up * playerSpeed; //Speed at which ball goes up//Your code
//                            Debug.Log("Up Swipe");
//                        }
//                    }
//                }
//            }
//        }
//    }
//}
