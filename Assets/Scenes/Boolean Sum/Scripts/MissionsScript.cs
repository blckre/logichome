﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class MissionsScript : MonoBehaviour
{
    public string tagName; //Tag based on mission

    /*
     * Create a list with the mission names
     * Then create a fxn that generates a random number and selects from the list created
     * This value is stored in a global variable and is used to pick what the player should be after some minutes
     * This would most likely be  aswitch statement to check if the player is equal to the mission value after the time has elapsed
     * the switch statement should be activated with a bool if true. i.e the bool sets to true if the mission value and player are the same. Then it checks for what to do 
     * This will eventually lead to the leaderboard being created. It will have the mission name and player. Leading the board is based on how many  types of mission you have completed which can be checked when the number is clicked.
     */

    public int missionNumber; //Number of the mission in the list
    private List<String> missionTitle = new List<String>
    {
        "Become an A in 45 seconds",
        "Become an A bar in 45 seconds",
        "Become a Zero in 45 seconds",
        "Become a One in 45 seconds"
    };


    public String MissionRandomiser()
    {
        missionNumber = UnityEngine.Random.Range(0, missionTitle.Count);

        String finalMission = missionTitle[missionNumber];

        switch (missionNumber)
        {
            case 0:
                tagName = "a";
                break;
            case 1:
                tagName = "abar";
                break;
            case 2:
                tagName = "zero";
                break;
            case 3:
                tagName = "one";
                break;
            default:
                break;
        }
        return finalMission;

    }
}
