﻿using UnityEngine;

public class BoolSumPlayer : MonoBehaviour
{
    public GameObject[] newPlayers; //The new object that occurs after a boolean calculation
    public Rigidbody2D rb;
    public Transform boxExplosion; //particle system
    public float playerSpeed;

    public int soundOn;
    public AudioSource popSound;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        soundOn = PlayerPrefs.GetInt("sound");
    }

    void LateUpdate()/*Later update, find a way to make the full object show or land on a platform instead*/
    {
        Vector3 pos = Camera.main.WorldToViewportPoint(transform.position); //Helps keep the player in the camera frame and not out of it
        pos.y = Mathf.Clamp(pos.y, 0.05f, 0.95f);   //Clamps the object betweeen two positions in the screen
        transform.position = Camera.main.ViewportToWorldPoint(pos);
    }

    void Update()
    {
        #region Windows
        float horizontalMovement = Input.GetAxis("Horizontal"); //Write code for touch screen devices too
        transform.Translate(horizontalMovement * playerSpeed * Time.deltaTime * Vector2.right);

        //Touch screen devices need a way to register up movement
        if (Input.GetKeyDown(KeyCode.UpArrow))
            rb.velocity += Vector2.up * playerSpeed; //Speed at which ball goes up
        #endregion

    }

    #region main game
    /*
     * The Boolean Algebra
     *  "A + A  = A",
        "A . A = A",
        "A . Abar  = 0",
        "A + Abar = 1",
        "A . 1  = A",
        "A + 1  = 1",
         "A . 0  = 0",
     *  "A + 0  = A",
        "Abar bar = A",
     */

    GameObject GetNewPlayer(string result)
    {
        foreach (var item in newPlayers)
        {
            if (item.CompareTag(result))
                return item; //checks the result with objects in the array and gives a result
        }
        return null;
    }

    void Calculation(GameObject player, GameObject collidedObject)
    {
        string value = player.tag;
        // Debug.Log("Calculating");

        switch (value)
        {
            case "a":

                if (collidedObject.CompareTag("dotabar"))
                {
                    string answer = "zero";
                    // Debug.Log(answer);
                    this.GetComponent<SpriteRenderer>().sprite = GetNewPlayer(answer).GetComponent<SpriteRenderer>().sprite;
                    this.tag = GetNewPlayer(answer).tag;

                }
                else if (collidedObject.CompareTag("plusabar"))
                {
                    string answer = "one";
                    // Debug.Log(answer);
                    this.GetComponent<SpriteRenderer>().sprite = GetNewPlayer(answer).GetComponent<SpriteRenderer>().sprite;
                    this.tag = GetNewPlayer(answer).tag;

                }
                else if (collidedObject.CompareTag("plusone"))
                {
                    string answer = "one";
                    // Debug.Log(answer);
                    this.GetComponent<SpriteRenderer>().sprite = GetNewPlayer(answer).GetComponent<SpriteRenderer>().sprite;
                    this.tag = GetNewPlayer(answer).tag;

                }
                else if (collidedObject.CompareTag("dotzero"))
                {
                    string answer = "zero";
                    // Debug.Log(answer);
                    this.GetComponent<SpriteRenderer>().sprite = GetNewPlayer(answer).GetComponent<SpriteRenderer>().sprite;
                    this.tag = GetNewPlayer(answer).tag;

                }
                break;

            case "abar":
                if (collidedObject.CompareTag("plusa"))
                {
                    string answer = "one";
                    //Debug.Log(answer);
                    this.GetComponent<SpriteRenderer>().sprite = GetNewPlayer(answer).GetComponent<SpriteRenderer>().sprite;
                    this.tag = GetNewPlayer(answer).tag;

                }
                else if (collidedObject.CompareTag("dota"))
                {
                    string answer = "zero";
                    //Debug.Log(answer);
                    this.GetComponent<SpriteRenderer>().sprite = GetNewPlayer(answer).GetComponent<SpriteRenderer>().sprite;
                    this.tag = GetNewPlayer(answer).tag;

                }
                else if (collidedObject.CompareTag("plusone"))
                {
                    string answer = "one";
                    // Debug.Log(answer);
                    this.GetComponent<SpriteRenderer>().sprite = GetNewPlayer(answer).GetComponent<SpriteRenderer>().sprite;
                    this.tag = GetNewPlayer(answer).tag;

                }
                else if (collidedObject.CompareTag("dotzero"))
                {
                    string answer = "zero";
                    // Debug.Log(answer);
                    this.GetComponent<SpriteRenderer>().sprite = GetNewPlayer(answer).GetComponent<SpriteRenderer>().sprite;
                    this.tag = GetNewPlayer(answer).tag;

                }
                break;

            case "zero":
                if (collidedObject.CompareTag("plusa"))
                {
                    string answer = "a";
                    // Debug.Log(answer);
                    this.GetComponent<SpriteRenderer>().sprite = GetNewPlayer(answer).GetComponent<SpriteRenderer>().sprite;
                    this.tag = GetNewPlayer(answer).tag;

                }
                else if (collidedObject.CompareTag("plusabar"))
                {
                    string answer = "abar";
                    // Debug.Log(answer);
                    this.GetComponent<SpriteRenderer>().sprite = GetNewPlayer(answer).GetComponent<SpriteRenderer>().sprite;
                    this.tag = GetNewPlayer(answer).tag;

                }
                else if (collidedObject.CompareTag("plusone"))
                {
                    string answer = "one";
                    // Debug.Log(answer);
                    this.GetComponent<SpriteRenderer>().sprite = GetNewPlayer(answer).GetComponent<SpriteRenderer>().sprite;
                    this.tag = GetNewPlayer(answer).tag;
                }
                break;

            case "one":
                if (collidedObject.CompareTag("dota"))
                {
                    string answer = "a";
                    //Debug.Log(answer);
                    this.GetComponent<SpriteRenderer>().sprite = GetNewPlayer(answer).GetComponent<SpriteRenderer>().sprite;
                    this.tag = GetNewPlayer(answer).tag;

                }
                else if (collidedObject.CompareTag("dotabar"))
                {
                    string answer = "abar";
                    //  Debug.Log(answer);
                    this.GetComponent<SpriteRenderer>().sprite = GetNewPlayer(answer).GetComponent<SpriteRenderer>().sprite;
                    this.tag = GetNewPlayer(answer).tag;

                }
                else if (collidedObject.CompareTag("plusone"))
                {
                    string answer = "one";
                    //   Debug.Log(answer);
                    this.GetComponent<SpriteRenderer>().sprite = GetNewPlayer(answer).GetComponent<SpriteRenderer>().sprite;
                    this.tag = GetNewPlayer(answer).tag;

                }
                else if (collidedObject.CompareTag("dotzero"))
                {
                    string answer = "zero";
                    //  Debug.Log(answer);
                    this.GetComponent<SpriteRenderer>().sprite = GetNewPlayer(answer).GetComponent<SpriteRenderer>().sprite;
                    this.tag = GetNewPlayer(answer).tag;

                }
                break;

            default:
                // Debug.Log("Invalid object");
                break;
        }

    }

    public void OnCollisionEnter2D(Collision2D collisions)
    {
        if (collisions.transform.CompareTag("walls"))
        {
            Debug.Log("Walls");//Game over
        }
        else if (!collisions.transform.CompareTag("base"))
        {
            // Debug.Log("Calculating");
            Instantiate(boxExplosion, collisions.transform.position, collisions.transform.rotation); // explosion takes place
            Calculation(this.gameObject, collisions.gameObject); //Check what has been collided with and take the appropriate actions
            if (soundOn == 2)
            {
                popSound.Play();
            }
            
            Destroy(collisions.gameObject);

        }

    }

    #endregion
}
