﻿using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public float timeRemaining = 50f;   //In seconds
    public bool timerIsRunning = false;
    public Text timeText;
    public float tutorialTime = 3f; //in seconds
    public GameObject missionPanel;
    public Text missionStatement;
    public Text FinalMis;
    public GameObject player;
    public MissionsScript ms;
    public GameObject wonPanel;
    public GameObject loseAPanel;
    public GameObject loseAbarPanel;
    public GameObject loseZeroPanel;
    public GameObject loseOnePanel;

    private void Start()
    {
        missionStatement.text = "" + ms.MissionRandomiser(); //Write the mission generated to the screen
        FinalMis.text = missionStatement.text;
        missionPanel.SetActive(true); // Mission for the game will be written here
        DisplayTime(timeRemaining);
    }
    void Update()
    {
        if (tutorialTime <= 0)
        {
            missionPanel.SetActive(false);
            //Tutorial panel active
        }
        else
            tutorialTime -= Time.deltaTime;


        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            timerIsRunning = true; //Start the timer
        }
        if (timerIsRunning)
        {
            if (timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
                DisplayTime(timeRemaining);
            }
            else
            {
                //Debug.Log("Time has run out!"); //Set the panel that checks if the mission was accomplished
                timeRemaining = 0;
                timerIsRunning = false;
                DisplayTime(timeRemaining);
                CompleteMission();
            }
        }
    }

    void DisplayTime(float timeToDisplay)
    {
        //timeToDisplay += 1;

        float minutes = Mathf.FloorToInt(timeToDisplay / 60); //Calculation
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);

        timeText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }

    void CompleteMission()
    {
        string quest = ms.tagName;
        if (player.CompareTag(quest))
        {
            //This is where data will be saved to user
            wonPanel.SetActive(true);

           PlayfabWeb.playStat.streakForBool++; //Increase the value íf the player wins
                                                         // Debug.Log(LoginWindowPlayfab.playStat.streakForBool);
                                                         //  Debug.Log("Passed the mission");
            PlayfabWeb.playStat.StartCloudUpdatePlayerStats(); //Updates the data in cloud

        }
        else
        {
            Debug.Log("Failed");
            if (quest == "a")
            {
                loseAPanel.SetActive(true);
            }
            else if (quest == "abar")
            {
                loseAbarPanel.SetActive(true);
            }
            else if (quest == "zero")
            {
                loseZeroPanel.SetActive(true);
            }
            else if (quest == "one")
            {
                loseOnePanel.SetActive(true);
            }
        }

    }
}
