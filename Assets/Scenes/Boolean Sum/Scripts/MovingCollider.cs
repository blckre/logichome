﻿using UnityEngine;

public class MovingCollider : MonoBehaviour
{

    public Transform cameraTransform;
    public float spriteHeight;

    void Start()
    {
        cameraTransform = Camera.main.transform;
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        spriteHeight = sr.sprite.bounds.size.y;
    }


    void Update()
    {
        if ((transform.position.y + spriteHeight - 1f) < (cameraTransform.position.y - 2f))
        {
            Vector3 newPosition = transform.position;
            newPosition.y += 2.48f * spriteHeight;
            transform.position = newPosition;
        }
    }
}
