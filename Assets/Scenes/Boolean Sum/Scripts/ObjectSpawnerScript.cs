﻿using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawnerScript : MonoBehaviour
{
    public float[] positionsOnXAxis; //Places where base can be spawned on x axis [7.2f, 4.59f, 1.97f, -0.64f, -5.65f, -3.18f]
    public SpawnRate[] spawnRates; // How many and in what time are created with the camera movement
    public GameObject[] bases; //Array containing all the bases
    public float currentTime; //Time of the game, calculated with the time at which the camera moves
    public List<float> remainingPos = new List<float>();//Where hasn't be spawned in recent times
    public List<GameObject> toBeDeletedObjects = new List<GameObject>(); //Objects to be deleted after the camera has passed the frame. Either time will be used to calculated when values enter this list or the distance of the camera
    public GameObject go;
    public float startPosX = 0.95f; //The very first spawn loaction on game start on x axis
    public float startPosY = -3.06f; //First spawn on y axis



    void BaseSpawning(float posX, float posY)
    {
        int randNum = Random.Range(0, bases.Length);
        go = Instantiate(bases[randNum], new Vector2(posX, posY), Quaternion.identity); //Spawn the base on the predefined x axis and y axis
        toBeDeletedObjects.Add(go); //Add the object to the list to regulated the destroying of the objects
    }

    void BaseWaveStart() /* Controls the spawning as the camera moves*/
    {
        remainingPos.AddRange(positionsOnXAxis); //Add the possible positions to the list
        int rateNo = Random.Range(0, spawnRates.Length); // the value of the speed and frequency of the level

        while (startPosY <= 80.0f)
        {
            for (int j = 0; j < spawnRates[rateNo].numberToSpawn; j++)
            {
                BaseSpawning(startPosX, startPosY);//Initial spawn on game start
                int left = Random.Range(0, remainingPos.Count); //Generate random index to check the positions list
                startPosX = remainingPos[left]; //New value of starting position on x axis
                //remainingPos.RemoveAt(left); //Remove the value since it has been used, prevents repetition. Has an index error currently
                startPosY += 1.8f; //Increment the position
   
            }
        }
    }

    public void DeleteSpawns()
    {
        for (int i = 0; i <= 8; i++)
        {
            Destroy(toBeDeletedObjects[i]); //Deletes the spawn
            toBeDeletedObjects.RemoveAt(i); //empties list
        }
    }

    void Start()
    {
        BaseWaveStart();
    }
    void Update()
    {
        BaseWaveStart();
    }
}

[System.Serializable]
public class SpawnRate
{
    public float rateOfSpawn;
    public float numberToSpawn;
}
//The spawn class containing time and amount is to release a certain number of blocks at a specific time and specific amount. 

