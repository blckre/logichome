﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class McqScript : MonoBehaviour
{

    public Text questions; //Question field in the display
    public Text optionA; //Writing to the text in the buttons
    public Text optionB;
    public Text optionC;
    public Text optionD;
    public Text scoreBoard; //Score board
    public Text timer;
    public Text questionCount; //Shows how many questions so far in the round
    public Text finalScore;
    public Text highestScore;

    public GameObject gameOverPanel;

   // public AudioSource wrongAnswer;

    private string answer; //This is the player's answer

    private float timerTillNxtQuestion = 50f;
    public int totalQuestionsAvailable = 31; //Gotten from unity
    public int totalQuestionsAsked = 0; //Limit is 12 questions
    public int totalScore = 0; //Total score at the end. Right answer is 10 points
    public int rand; // Making it global so it can be used to check answer

    public List<QuestionsList> questionData = new List<QuestionsList>(); //A list of the questions available 
    public List<int> questionsAskedAlready = new List<int>(); //A list of questions that have already been asked by the game

    private void Start()
    {
        QuestionGen(questionData); //Call the first question
        scoreBoard.text = "" + totalScore;
        DisplayTime(timerTillNxtQuestion);
    }

    private void Update()
    {
       
        if (totalQuestionsAsked > 12)
        {
            StopCoroutine(Clock()); //Stop coroutine
            finalScore.text = totalScore.ToString();
            gameOverPanel.SetActive(true);
            PlayfabWeb.playStat.mcqScore = totalScore; //Set leaderboard
            PlayfabWeb.playStat.StartCloudUpdatePlayerStats();
            Debug.Log(PlayfabWeb.playStat.mcqScore);

        }
        else
            StartCoroutine(Clock());

        DisplayTime(timerTillNxtQuestion);
    }

    public void QuestionGen(List<QuestionsList> data)
    {
        timerTillNxtQuestion = 50f;
        totalQuestionsAsked++; //Adding the number
        rand = Random.Range(0, data.Count); //A random question
        questionsAskedAlready.Add(rand);


        while (questionsAskedAlready.Contains(rand))
        {
            rand = Random.Range(0, data.Count);
        }


        questions.text = data[rand].question;
        optionA.text = data[rand].answerA;
        optionB.text = data[rand].answerB;
        optionC.text = data[rand].answerC;
        optionD.text = data[rand].answerD;
        questionCount.text = totalQuestionsAsked.ToString() + "/12";

    }

    public void OptionA()
    {
        answer = optionA.text;
        CheckAnswer(answer, questionData); //Check if it is correct
    }

    public void OptionB()
    {
        answer = optionB.text;
        CheckAnswer(answer, questionData); //Check if it is correct
    }

    public void OptionC()
    {
        answer = optionC.text;
        CheckAnswer(answer, questionData); //Check if it is correct
    }

    public void OptionD()
    {
        answer = optionD.text;
        CheckAnswer(answer, questionData); //Check if it is correct
    }

    public void CheckAnswer(string selectedAnswer, List<QuestionsList> data)
    {

        if (selectedAnswer == data[rand].correctAnswer)
        {
            //Debug.Log("Correct Answer");
            AddScore();
            QuestionGen(questionData);
            //Display new questions
        }
        else
        {
            //Debug.Log("Wrong Answer");
            QuestionGen(questionData);
           // wrongAnswer.Play();
            //Display new question
            //play buzzer sound

        }

    }

    public void AddScore()
    {
        totalScore += 10; //Adding score
                          // Debug.Log(totalScore);
        scoreBoard.text = "" + totalScore; //Display score on score board
    }

    public void NextButton()
    {
        QuestionGen(questionData);
    }

    void DisplayTime(float timeToDisplay)
    {
        //timeToDisplay += 1;

        float minutes = Mathf.FloorToInt(timeToDisplay / 60); //Calculation
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);

        timer.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }

    IEnumerator Clock()
    {
        yield return new WaitForSeconds(1f); //This is the one second count
        timerTillNxtQuestion -= Time.deltaTime;
        if (Mathf.Round(timerTillNxtQuestion) == 0)
            QuestionGen(questionData); //Generate new question once time is over

    }

}

[System.Serializable]
public class QuestionsList
{
    public string question;
    public string answerA;
    public string answerB;
    public string answerC;
    public string answerD;
    public string correctAnswer;

}