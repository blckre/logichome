﻿using PlayFab;
using PlayFab.ClientModels;
using PlayFab.Json;
using UnityEngine;

public class PlayfabWeb : MonoBehaviour
{
    public static PlayfabWeb playStat;

    public int mcqScore; //Multiple choice score
    public int streakForBool; //How many times player was able to do the boolean algebra

    //private void OnEnable()
    //{
    //    if (PlayfabWeb.playStat == null)
    //    {
    //        PlayfabWeb.playStat = this;
    //    }
    //    else
    //    {
    //        if (PlayfabWeb.playStat != this)
    //        {
    //           Destroy(this.gameObject);
    //        }
    //    }
    //    DontDestroyOnLoad(this.gameObject);
    //    //Ensures there is only on play stat throughout the entire game. A singleton
    //}

    //public void SetStats()
    //{
    //    PlayFabClientAPI.UpdatePlayerStatistics
    //    (new UpdatePlayerStatisticsRequest
    //    {
    //        // request.Statistics is a list, so multiple StatisticUpdate objects can be defined if required.
    //        Statistics = new List<StatisticUpdate>
    //        {
    //        new StatisticUpdate { StatisticName = "Boolean Algebra", Value = boolLevel },
    //        new StatisticUpdate { StatisticName = "Number System", Value = mcqLevel},
    //        new StatisticUpdate { StatisticName = "MCQ Score", Value = mcqScore },
    //        new StatisticUpdate { StatisticName = "Bool Score", Value = streakForBool},
    //        }
    //    },
    //    result => { Debug.Log("User statistics updated"); },
    //    error => { Debug.LogError(error.GenerateErrorReport()); }
    //    );

    //    //Setting the name of the statistics to a header name. From the documentation
    //}

    public void GetStats()
    {
        PlayFabClientAPI.GetPlayerStatistics(
            new GetPlayerStatisticsRequest(),
            OnGetStats,
            error => Debug.LogError(error.GenerateErrorReport())
        );
    }

    void OnGetStats(GetPlayerStatisticsResult result)
    {
        //Debug.Log("Received the following Statistics:");
        foreach (var eachStat in result.Statistics)
        {
            // Debug.Log("Statistic (" + eachStat.StatisticName + "): " + eachStat.Value);
            switch (eachStat.StatisticName)
            {
                case "MCQ Score":
                    mcqScore = eachStat.Value;
                    break;
                case "Bool Score":
                    streakForBool = eachStat.Value;
                    break;

            }
        }
    }

    // Build the request object and access the API
    public void StartCloudUpdatePlayerStats()
    {
        PlayFabClientAPI.ExecuteCloudScript(new ExecuteCloudScriptRequest()
        {
            FunctionName = "UpdatePlayerStats", // Calling the function inthe cloud script Revision2
            FunctionParameter = new { McqScore = mcqScore, StreakForBool = streakForBool }, // The parameter provided to your function
            GeneratePlayStreamEvent = true, // Optional - Shows this event in PlayStream
        }, OnCloudUpdatePlayerStats, OnErrorShared);
    }
    // OnCloudHelloWorld defined in the next code block

    private static void OnCloudUpdatePlayerStats(ExecuteCloudScriptResult result)
    {
        // CloudScript returns arbitrary results, so you have to evaluate them one step and one parameter at a time
        //  Debug.Log(PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer));
        JsonObject jsonResult = (JsonObject)result.FunctionResult;
        jsonResult.TryGetValue("messageValue", out object messageValue); // note how "messageValue" directly corresponds to the JSON values set in CloudScript
                                                                         // Debug.Log((string)messageValue);
    }

    private static void OnErrorShared(PlayFabError error)
    {
        //Debug.Log(error.GenerateErrorReport());
    }
  
}
