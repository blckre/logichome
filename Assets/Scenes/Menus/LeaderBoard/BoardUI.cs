﻿using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BoardUI : MonoBehaviour
{
    public Button mcqButton;
    public Button boolButton;
    public GameObject mcqPanel;
    public GameObject boolPanel;
    public GameObject leaderListPrefab; //Contains name and score
    public Transform listContainer;

    public void MCQPanel(GetLeaderboardResult result)
    {
        boolPanel.SetActive(false);
        mcqPanel.SetActive(true);
        foreach (PlayerLeaderboardEntry player in result.Leaderboard)
        {
            GameObject tempList = Instantiate(leaderListPrefab, listContainer);
            LeaderList ll = tempList.GetComponent<LeaderList>();
            ll.playerName.text = player.DisplayName;
            ll.playerScore.text = player.StatValue.ToString();
            //  Debug.Log(player.DisplayName + ": " + player.StatValue);
        }

    }

    public void BoolPanel(GetLeaderboardResult result)
    {
        mcqPanel.SetActive(false);
        boolPanel.SetActive(true);

        foreach (PlayerLeaderboardEntry player in result.Leaderboard)
        {
            GameObject tempList = Instantiate(leaderListPrefab, listContainer);
            LeaderList ll = tempList.GetComponent<LeaderList>();
            ll.playerName.text = player.DisplayName;
            ll.playerScore.text = player.StatValue.ToString();
            //Debug.Log(player.DisplayName + ": " + player.StatValue);
        }
    }

    public void GetBoolLeaderboard()
    {
        //Getting the value for bool
        var requestLeaderboard = new GetLeaderboardRequest { StartPosition = 0, StatisticName = "Bool Score", MaxResultsCount = 40 };
        PlayFabClientAPI.GetLeaderboard(requestLeaderboard, BoolPanel, OnErrorLeaderboard);
    }

    public void GetMCQLeaderboard()
    {
        //Getting the value for mcq
        var requestLeaderboard = new GetLeaderboardRequest { StartPosition = 0, StatisticName = "MCQ Score", MaxResultsCount = 40 };
        PlayFabClientAPI.GetLeaderboard(requestLeaderboard, MCQPanel, OnErrorLeaderboard);
    }

    public void CloseLeaderboardPanel()
    {
        //Fxn when closing leaderboard scene
        for (int i = listContainer.childCount - 1; i >= 0; i--)
        {
            Destroy(listContainer.GetChild(i).gameObject);
        }

        SceneManager.LoadScene("MainMenu");
    }
    void OnErrorLeaderboard(PlayFabError error)
    {
        // Debug.LogError(error.GenerateErrorReport());
    }


}
