﻿using UnityEngine;
using UnityEngine.UI;

public class Settings : MonoBehaviour
{
    [SerializeField]
    private Toggle musicToggle;

    [SerializeField]
    private AudioSource gameMusic;

    [SerializeField]
    private Toggle soundToggle;

    [SerializeField]
    private AudioSource gameSound;


    public void Awake()
    {
        if (!PlayerPrefs.HasKey("music"))
        {
            PlayerPrefs.SetInt("music", 1);
            musicToggle.isOn = true;
            gameMusic.enabled = true;
            PlayerPrefs.Save();
        }
        else
        {
            if (PlayerPrefs.GetInt("music") == 0)
            {
                gameMusic.enabled = false;
                musicToggle.isOn = false;
            }
            else
            {
                gameMusic.enabled = true;
                musicToggle.isOn = true;
            }
        }


        if (!PlayerPrefs.HasKey("sound"))
        {
            PlayerPrefs.SetInt("sound", 2);
            soundToggle.isOn = true;
            gameSound.enabled = true;
            PlayerPrefs.Save();
        }
        else
        {
            if (PlayerPrefs.GetInt("sound") == 0)
            {
                gameSound.enabled = false;
                soundToggle.isOn = false;
            }
            else
            {
                gameSound.enabled = true;
                soundToggle.isOn = true;
            }
        }
    }

    public void ChangeMusicToggle()
    {
        if (musicToggle.isOn)
        {
            PlayerPrefs.SetInt("music", 1);
            gameMusic.enabled = true;
        }
        else
        {
            PlayerPrefs.SetInt("music", 0);
            gameMusic.enabled = false;
        }
        PlayerPrefs.Save();
    }

    public void ChangeSoundToggle()
    {
        if (soundToggle.isOn)
        {
            PlayerPrefs.SetInt("sound", 2);
            gameSound.enabled = true;
        }
        else
        {
            PlayerPrefs.SetInt("sound", 0);
            gameSound.enabled = false;
        }
        PlayerPrefs.Save();
    }
    //In other scenes, check if sound is on then , play sound
    /* public int soundOn;
     * public int musicOn;
     * [SerializeField]
    private AudioSource gameMusic;
     [SerializeField]
    private AudioSource gameSound;
     * 
     * void Start()
     * {
     * soundOn = PlayerPrefs.GetInt("sound");
     * musicOn = PlayerPrefs.GetInt("music");
     * }
     * void Update()
     * {
     * if(soundOn == 2)
     * {
     *  gameSound.enabled = true;
     *  }
     *  else
     *  gameSound.enabled = false;
     * 
     * if(musicOn == 1)
     * {
     *  gameMusic.enabled = true;
     *  }
     *  else
     *   gameMusic.enabled = false;
     *  }
     */
}
