﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingScript : MonoBehaviour
{
    public int i;

    void Start()
    {
        i = 0;
    }

    void Update()
    {
        i++;

        if (i == 100)
        {
            SceneManager.LoadScene("CreateAndLogin");
        }
    }
}
