﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonsScript : MonoBehaviour
{
    public GameObject quitPanel;

    public void Home()
    {
        if (PlayfabWeb.playStat.mcqScore != 0 && PlayfabWeb.playStat.streakForBool != 0)
            SceneManager.LoadScene("MainMenuLigh");
        else
            SceneManager.LoadScene("MainMenu");
    }
    public void Play()
    {
        if(PlayfabWeb.playStat.mcqScore != 0 && PlayfabWeb.playStat.streakForBool != 0)
            SceneManager.LoadScene("SelectRoomSceneLigh");
        else
            SceneManager.LoadScene("SelectRoomScene");
        // Debug.Log("Playing..");
    }
    public void LeaderBoard()
    {
        SceneManager.LoadScene("LeaderBoardScene");
    }
    public void Settings()
    {
        SceneManager.LoadScene("SettingsScene");
    }

    public void GuestRoomScenes()
    {
        SceneManager.LoadScene("FirstMCQ");
    }
    public void DiningScenes()
    {
        SceneManager.LoadScene("Level 1");
    }
    public void KitchenScenes()
    {
        SceneManager.LoadScene("");
    }

    public void QuitPanel()
    {
        quitPanel.SetActive(true);
    }

    public void QuitPanelOff()
    {
        quitPanel.SetActive(false);
    }
    public void FinalQuit()
    {
        Debug.Log("Application quit");
        Application.Quit();
    }


    public void ExittoRoom()
    {
        if (PlayfabWeb.playStat.mcqScore != 0 && PlayfabWeb.playStat.streakForBool != 0)
            SceneManager.LoadScene("SelectRoomSceneLigh");
        else
            SceneManager.LoadScene("SelectRoomScene");
    }
}
