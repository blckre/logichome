﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class VideoScript : MonoBehaviour
{
    public VideoPlayer video;
    

    private void Update()
    {
        if (video.frame > 0 && (video.isPlaying == false))
          SceneManager.LoadScene("CreateAndLogin");
        
    }

    public void Skip()
    {
        SceneManager.LoadScene("CreateAndLogin");
    }
}
