﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 <McqScore>j__TPar <>f__AnonymousType0`2::get_McqScore()
// 0x00000002 <StreakForBool>j__TPar <>f__AnonymousType0`2::get_StreakForBool()
// 0x00000003 System.Void <>f__AnonymousType0`2::.ctor(<McqScore>j__TPar,<StreakForBool>j__TPar)
// 0x00000004 System.Boolean <>f__AnonymousType0`2::Equals(System.Object)
// 0x00000005 System.Int32 <>f__AnonymousType0`2::GetHashCode()
// 0x00000006 System.String <>f__AnonymousType0`2::ToString()
// 0x00000007 System.Void BoolSumPlayer::Start()
extern void BoolSumPlayer_Start_mCEB8ED6BB6B96328FA690D94F0BB2648D750DC7D ();
// 0x00000008 System.Void BoolSumPlayer::LateUpdate()
extern void BoolSumPlayer_LateUpdate_m049F8403266B73BEF9C443C15DE8F624D782864C ();
// 0x00000009 System.Void BoolSumPlayer::Update()
extern void BoolSumPlayer_Update_m2E6E222EF8D6D1B9CA420C685275FBBC8F616C2D ();
// 0x0000000A UnityEngine.GameObject BoolSumPlayer::GetNewPlayer(System.String)
extern void BoolSumPlayer_GetNewPlayer_m47EC900A444128A1711D2DF75393F535442132A5 ();
// 0x0000000B System.Void BoolSumPlayer::Calculation(UnityEngine.GameObject,UnityEngine.GameObject)
extern void BoolSumPlayer_Calculation_mCAE7FDEFF8B3A89746987D306D98A30F54C2D3A1 ();
// 0x0000000C System.Void BoolSumPlayer::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void BoolSumPlayer_OnCollisionEnter2D_m39C632819970C8FA198158B317BF620526CD14BC ();
// 0x0000000D System.Void BoolSumPlayer::.ctor()
extern void BoolSumPlayer__ctor_mE2ECB0C2FB1A27C8B9C462AF87DB603CF171676E ();
// 0x0000000E System.Void CameraControl::Start()
extern void CameraControl_Start_m77CCD413451A457E13B0F9D4EBB0D76633CEC5C1 ();
// 0x0000000F System.Void CameraControl::Update()
extern void CameraControl_Update_m4BD3B811296B1A0E5C8FF3148DC11622A6430F6F ();
// 0x00000010 System.Collections.IEnumerator CameraControl::DeleteBoard()
extern void CameraControl_DeleteBoard_mE895591B51C9038849261B1086F88207DF8D0082 ();
// 0x00000011 System.Void CameraControl::.ctor()
extern void CameraControl__ctor_m67DAD57065E2F985D5F050A4DDD3DB9B95548100 ();
// 0x00000012 System.Void CameraControl_<DeleteBoard>d__7::.ctor(System.Int32)
extern void U3CDeleteBoardU3Ed__7__ctor_mBE59F1CB4F1E20C7B6438FF842C58540FA559673 ();
// 0x00000013 System.Void CameraControl_<DeleteBoard>d__7::System.IDisposable.Dispose()
extern void U3CDeleteBoardU3Ed__7_System_IDisposable_Dispose_m37213975F4ACA7036620FA1830FE4CA26849D3D5 ();
// 0x00000014 System.Boolean CameraControl_<DeleteBoard>d__7::MoveNext()
extern void U3CDeleteBoardU3Ed__7_MoveNext_mF79C8FCEEEAC8963EC510A17389E9B2C2AFBB345 ();
// 0x00000015 System.Object CameraControl_<DeleteBoard>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDeleteBoardU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m630FEAC85049F2A8BDEE2F6C3D14C6AA187C454C ();
// 0x00000016 System.Void CameraControl_<DeleteBoard>d__7::System.Collections.IEnumerator.Reset()
extern void U3CDeleteBoardU3Ed__7_System_Collections_IEnumerator_Reset_m49D517EAAEF0087714E9AD0F57A53C5F3E0458A6 ();
// 0x00000017 System.Object CameraControl_<DeleteBoard>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CDeleteBoardU3Ed__7_System_Collections_IEnumerator_get_Current_mE17AAA7B2A7796ED2134AA80EC68F13F366BFDC1 ();
// 0x00000018 System.Void GameManager::Start()
extern void GameManager_Start_m26461AEF27E44DB8FECCBC19D6C9E228B658BF8E ();
// 0x00000019 System.Void GameManager::Update()
extern void GameManager_Update_mC9303BA7C3117BD861F49F8E36151CC52117E6C1 ();
// 0x0000001A System.Void GameManager::DisplayTime(System.Single)
extern void GameManager_DisplayTime_m7DEC035DE877FFD5EA6BD7EF33593CEF83A25475 ();
// 0x0000001B System.Void GameManager::CompleteMission()
extern void GameManager_CompleteMission_m25F8091EEABBC1F93B6DED280C2067355D1A7217 ();
// 0x0000001C System.Void GameManager::.ctor()
extern void GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D ();
// 0x0000001D System.String MissionsScript::MissionRandomiser()
extern void MissionsScript_MissionRandomiser_m0022AC1AF800608888E77E9B0B6B8BACB6BDC37E ();
// 0x0000001E System.Void MissionsScript::.ctor()
extern void MissionsScript__ctor_mC72E74999EEE1FDBA20FCC365BDE927DD520BF8B ();
// 0x0000001F System.Void MovingCollider::Start()
extern void MovingCollider_Start_mB4BC3EA0F3410BFCF31D3A90F50ADC166DC57718 ();
// 0x00000020 System.Void MovingCollider::Update()
extern void MovingCollider_Update_m828EDD50635EA4A13D62B81B9B9C72E9A708FCCB ();
// 0x00000021 System.Void MovingCollider::.ctor()
extern void MovingCollider__ctor_m5C790BF66AA7688A8E4D8D97BD660AE86F70BFDD ();
// 0x00000022 System.Void ObjectSpawnerScript::BaseSpawning(System.Single,System.Single)
extern void ObjectSpawnerScript_BaseSpawning_mB8ED61311424EA26F8286BDDF9212188E84DF4B6 ();
// 0x00000023 System.Void ObjectSpawnerScript::BaseWaveStart()
extern void ObjectSpawnerScript_BaseWaveStart_m9C7C4969CEABBE5E005D43240245D5027BFD8D22 ();
// 0x00000024 System.Void ObjectSpawnerScript::DeleteSpawns()
extern void ObjectSpawnerScript_DeleteSpawns_m3C962DF2F24E6F05BC4F7C522062C31AB9755AD5 ();
// 0x00000025 System.Void ObjectSpawnerScript::Start()
extern void ObjectSpawnerScript_Start_mA07578CBC153796A1DFF1E10658C006E46465BE3 ();
// 0x00000026 System.Void ObjectSpawnerScript::Update()
extern void ObjectSpawnerScript_Update_mD89DC6EA967F2B51A5D2F7EB98D78252B7DE4F1B ();
// 0x00000027 System.Void ObjectSpawnerScript::.ctor()
extern void ObjectSpawnerScript__ctor_m03D588905A1E88882F6F1E22D9CCD34E3E9C95B7 ();
// 0x00000028 System.Void SpawnRate::.ctor()
extern void SpawnRate__ctor_mFF26BE9EBFE3DFD14FD12007A7AAFCD586BBC1B6 ();
// 0x00000029 System.Void TouchScreen::Update()
extern void TouchScreen_Update_m20C1CF1312B2FC6A3E52947B41EE3B27CB89C1FE ();
// 0x0000002A System.Void TouchScreen::.ctor()
extern void TouchScreen__ctor_m3DB45801B2C8CC442B1DFEAF8F0A75B1545B973E ();
// 0x0000002B System.Void AuthService::add_OnDisplayAuthentication(AuthService_DisplayAuthenticationEvent)
extern void AuthService_add_OnDisplayAuthentication_mB030003EB4E0A840A5ECFA4EC9911EB4AC80AFA7 ();
// 0x0000002C System.Void AuthService::remove_OnDisplayAuthentication(AuthService_DisplayAuthenticationEvent)
extern void AuthService_remove_OnDisplayAuthentication_mCE1875B9CEF3DA374196DDB63C9E791623489606 ();
// 0x0000002D System.Void AuthService::add_OnLoginSuccess(AuthService_LoginSuccessEvent)
extern void AuthService_add_OnLoginSuccess_m7CB60D72C0F18B5696E53DBD1724CD56C1D77B2D ();
// 0x0000002E System.Void AuthService::remove_OnLoginSuccess(AuthService_LoginSuccessEvent)
extern void AuthService_remove_OnLoginSuccess_mD65EA9217985E6C9656DE8D00AEB20E6C4A922ED ();
// 0x0000002F System.Void AuthService::add_OnPlayFabError(AuthService_PlayFabErrorEvent)
extern void AuthService_add_OnPlayFabError_mFC753F219C54BEFA34EAF87E1249DA8DA20A7C78 ();
// 0x00000030 System.Void AuthService::remove_OnPlayFabError(AuthService_PlayFabErrorEvent)
extern void AuthService_remove_OnPlayFabError_m14A39214E13C2B0AEF50228F40884EFCAC3A428D ();
// 0x00000031 System.String AuthService::get_PlayFabId()
extern void AuthService_get_PlayFabId_m64252895061CC0311B4E900EED7B9FECCF9E160C ();
// 0x00000032 System.String AuthService::get_SessionTicket()
extern void AuthService_get_SessionTicket_m0EC3EB5DB640E30A3C41D61A2F3A8B722C511574 ();
// 0x00000033 AuthService AuthService::get_Instance()
extern void AuthService_get_Instance_m3FB7F648BD0BC4E1799DBE249F553053D055114D ();
// 0x00000034 System.Void AuthService::.ctor()
extern void AuthService__ctor_m0623CA575881E4EA27E6EBB5AD6272D0A91645A0 ();
// 0x00000035 System.Boolean AuthService::get_RememberMe()
extern void AuthService_get_RememberMe_mCC09C5C31DA042632C9358C6583B9D3F73A9B758 ();
// 0x00000036 System.Void AuthService::set_RememberMe(System.Boolean)
extern void AuthService_set_RememberMe_m9A656CAA8ECEBF780E5EC64F2D5A35031B8EF8FA ();
// 0x00000037 Authtypes AuthService::get_AuthType()
extern void AuthService_get_AuthType_mDC2915DB955E2A41ACC2D067EFD42E1C7DCA5DCF ();
// 0x00000038 System.Void AuthService::set_AuthType(Authtypes)
extern void AuthService_set_AuthType_m845215E4A772203B4FD23A6A944A61D35617D6BB ();
// 0x00000039 System.String AuthService::get_RememberMeId()
extern void AuthService_get_RememberMeId_m572AB71FC8D518FB0B29C947175564FD81A2E548 ();
// 0x0000003A System.Void AuthService::set_RememberMeId(System.String)
extern void AuthService_set_RememberMeId_m057CD235046D5A5506FD628AE75CB7EA5C2D8899 ();
// 0x0000003B System.Void AuthService::ClearRememberMe()
extern void AuthService_ClearRememberMe_mC36914419ACBBB6DAEFF142D34137D18DDD7E8A0 ();
// 0x0000003C System.Void AuthService::Authenticate(Authtypes)
extern void AuthService_Authenticate_mDB8E51592BE3841BCF347D5E8A626ABDD00FD2AE ();
// 0x0000003D System.Void AuthService::Authenticate()
extern void AuthService_Authenticate_mF215D4E5E616A7FF9C34997377A5A046F85C72B9 ();
// 0x0000003E System.Void AuthService::AuthenticateEmailPassword()
extern void AuthService_AuthenticateEmailPassword_mCB6A295C25A1B858DFF867BA97039C592E7AB3F4 ();
// 0x0000003F System.Void AuthService::AddAccountAndPassword()
extern void AuthService_AddAccountAndPassword_mFC617E53AB774910FB11E035F46354BC14C1B1AE ();
// 0x00000040 System.Void AuthService::SilentlyAuthenticate(System.Action`1<PlayFab.ClientModels.LoginResult>)
extern void AuthService_SilentlyAuthenticate_m4DE0BDC374255862B7815E9EAC8E56E62855DE18 ();
// 0x00000041 System.Void AuthService::UnlinkSilentAuth()
extern void AuthService_UnlinkSilentAuth_m02CBA8730DBB4AF98A946606845B914301A5E7AD ();
// 0x00000042 System.Void AuthService::<AuthenticateEmailPassword>b__43_2(PlayFab.ClientModels.LoginResult)
extern void AuthService_U3CAuthenticateEmailPasswordU3Eb__43_2_mCB05DE9FF60C6A0A5AC52B443C205B641DEDF0CA ();
// 0x00000043 System.Void AuthService::<AddAccountAndPassword>b__44_0(PlayFab.ClientModels.LoginResult)
extern void AuthService_U3CAddAccountAndPasswordU3Eb__44_0_m88FE57B28A7397CF8E102328C204E236E1C09E3D ();
// 0x00000044 System.Void AuthService_DisplayAuthenticationEvent::.ctor(System.Object,System.IntPtr)
extern void DisplayAuthenticationEvent__ctor_mFE2A0C7477EC532A3BF5D959173D01201994A817 ();
// 0x00000045 System.Void AuthService_DisplayAuthenticationEvent::Invoke()
extern void DisplayAuthenticationEvent_Invoke_m43469A76922AA592F0257615E4FE6E1DECAF0DB6 ();
// 0x00000046 System.IAsyncResult AuthService_DisplayAuthenticationEvent::BeginInvoke(System.AsyncCallback,System.Object)
extern void DisplayAuthenticationEvent_BeginInvoke_m682896BED0B790EE97E3C82C0F49F2997B3B7A7A ();
// 0x00000047 System.Void AuthService_DisplayAuthenticationEvent::EndInvoke(System.IAsyncResult)
extern void DisplayAuthenticationEvent_EndInvoke_m5C68F299EFD1C9509E13ABD79758F1972A4A4302 ();
// 0x00000048 System.Void AuthService_LoginSuccessEvent::.ctor(System.Object,System.IntPtr)
extern void LoginSuccessEvent__ctor_m8C0E4C223A07DE08F432F587611D2D594B5877A6 ();
// 0x00000049 System.Void AuthService_LoginSuccessEvent::Invoke(PlayFab.ClientModels.LoginResult)
extern void LoginSuccessEvent_Invoke_m928B62C46B7CE5AF3242BA6CB1BA84E291F091A4 ();
// 0x0000004A System.IAsyncResult AuthService_LoginSuccessEvent::BeginInvoke(PlayFab.ClientModels.LoginResult,System.AsyncCallback,System.Object)
extern void LoginSuccessEvent_BeginInvoke_m9D2EED28830762BD55C4564B1AA652CD115A36D1 ();
// 0x0000004B System.Void AuthService_LoginSuccessEvent::EndInvoke(System.IAsyncResult)
extern void LoginSuccessEvent_EndInvoke_m540A997912D92B52DBA50C3997FF7752C5ADE61B ();
// 0x0000004C System.Void AuthService_PlayFabErrorEvent::.ctor(System.Object,System.IntPtr)
extern void PlayFabErrorEvent__ctor_m8AAE0396F2E88C9A50BE86FA307B60C235E4209C ();
// 0x0000004D System.Void AuthService_PlayFabErrorEvent::Invoke(PlayFab.PlayFabError)
extern void PlayFabErrorEvent_Invoke_mE6E7170DB000147693299FDB334AE2CE2D6F203B ();
// 0x0000004E System.IAsyncResult AuthService_PlayFabErrorEvent::BeginInvoke(PlayFab.PlayFabError,System.AsyncCallback,System.Object)
extern void PlayFabErrorEvent_BeginInvoke_m693F00F683A07C4554137803F310E310FAC8A3BA ();
// 0x0000004F System.Void AuthService_PlayFabErrorEvent::EndInvoke(System.IAsyncResult)
extern void PlayFabErrorEvent_EndInvoke_m40E31113991BF65DBDDF30FA87719980A7FE2D24 ();
// 0x00000050 System.Void AuthService_<>c::.cctor()
extern void U3CU3Ec__cctor_mE4E23231CC95C6602664DA4AF7EEC0C4377BC8EC ();
// 0x00000051 System.Void AuthService_<>c::.ctor()
extern void U3CU3Ec__ctor_m8CA9F89CE5483E899543F5AD191A68966FD4C2D5 ();
// 0x00000052 System.Void AuthService_<>c::<AuthenticateEmailPassword>b__43_0(PlayFab.ClientModels.LoginResult)
extern void U3CU3Ec_U3CAuthenticateEmailPasswordU3Eb__43_0_mB783A9A5F580BE386CB8D942795ADCE63D8B3101 ();
// 0x00000053 System.Void AuthService_<>c::<AuthenticateEmailPassword>b__43_1(PlayFab.PlayFabError)
extern void U3CU3Ec_U3CAuthenticateEmailPasswordU3Eb__43_1_m430BED5854C55C79341EBC51584B447B6228BC11 ();
// 0x00000054 System.Void AuthService_<>c::<AuthenticateEmailPassword>b__43_3(PlayFab.PlayFabError)
extern void U3CU3Ec_U3CAuthenticateEmailPasswordU3Eb__43_3_mB1D530369E773A809663B7648261B724E2A4FB4A ();
// 0x00000055 System.Void AuthService_<>c::<AddAccountAndPassword>b__44_2(PlayFab.PlayFabError)
extern void U3CU3Ec_U3CAddAccountAndPasswordU3Eb__44_2_mD20B916F7B2B711359753C2C1232021F8DB970F8 ();
// 0x00000056 System.Void AuthService_<>c::<UnlinkSilentAuth>b__46_0(PlayFab.ClientModels.LoginResult)
extern void U3CU3Ec_U3CUnlinkSilentAuthU3Eb__46_0_mB9D2B53EC27797CD8F37D1500869D37821CB2794 ();
// 0x00000057 System.Void AuthService_<>c__DisplayClass44_0::.ctor()
extern void U3CU3Ec__DisplayClass44_0__ctor_mF6DD4D1975C17D289BB32AF01F902B8FCED156E3 ();
// 0x00000058 System.Void AuthService_<>c__DisplayClass44_0::<AddAccountAndPassword>b__1(PlayFab.ClientModels.AddUsernamePasswordResult)
extern void U3CU3Ec__DisplayClass44_0_U3CAddAccountAndPasswordU3Eb__1_m075298722917038E3AA4446368E8A34D9611B3ED ();
// 0x00000059 System.Void AuthService_<>c__DisplayClass45_0::.ctor()
extern void U3CU3Ec__DisplayClass45_0__ctor_mAEC48DD405E3E2CEAD65C6CBF74BB7EA2BC49549 ();
// 0x0000005A System.Void AuthService_<>c__DisplayClass45_0::<SilentlyAuthenticate>b__0(PlayFab.ClientModels.LoginResult)
extern void U3CU3Ec__DisplayClass45_0_U3CSilentlyAuthenticateU3Eb__0_mCCFA24C6153D7244619FE00F7C41D52C29E71D10 ();
// 0x0000005B System.Void AuthService_<>c__DisplayClass45_0::<SilentlyAuthenticate>b__1(PlayFab.PlayFabError)
extern void U3CU3Ec__DisplayClass45_0_U3CSilentlyAuthenticateU3Eb__1_m4B2A81C9B2481F6FD433B928EEFDA13A885C68E4 ();
// 0x0000005C System.Void LoginWindowPlayfab::Awake()
extern void LoginWindowPlayfab_Awake_m80FF93415DFB578A3EFD38B97F4F7E7C85460847 ();
// 0x0000005D System.Void LoginWindowPlayfab::Start()
extern void LoginWindowPlayfab_Start_mB6D25C0B9FB8CFA1AF9A1FEE06FC96A3A9C718D3 ();
// 0x0000005E System.Void LoginWindowPlayfab::OnLoginSuccess(PlayFab.ClientModels.LoginResult)
extern void LoginWindowPlayfab_OnLoginSuccess_m5F2F6603E63FAA895C4B65159DD8077B2F021BFF ();
// 0x0000005F System.Void LoginWindowPlayfab::OnDisplayName(PlayFab.ClientModels.UpdateUserTitleDisplayNameResult)
extern void LoginWindowPlayfab_OnDisplayName_mBDD55EBC6D85E6DE137401299FF2492DB18D6D8A ();
// 0x00000060 System.Void LoginWindowPlayfab::OnPlayFaberror(PlayFab.PlayFabError)
extern void LoginWindowPlayfab_OnPlayFaberror_mA8E385ACFAFB22A577E656C695DF198906EE3022 ();
// 0x00000061 System.Void LoginWindowPlayfab::OnDisplayAuthentication()
extern void LoginWindowPlayfab_OnDisplayAuthentication_m82D4BB0D9578E5F3A4C12F4B6CC819433945A94B ();
// 0x00000062 System.Void LoginWindowPlayfab::OnPlayAsGuestClicked()
extern void LoginWindowPlayfab_OnPlayAsGuestClicked_mADF839C900EB8D9A4AB984C7A05E590288152527 ();
// 0x00000063 System.Void LoginWindowPlayfab::OnLoginClicked()
extern void LoginWindowPlayfab_OnLoginClicked_m5625731F3EBDEAD529F237AEA999B849967F5508 ();
// 0x00000064 System.Void LoginWindowPlayfab::OnRegisterButtonClicked()
extern void LoginWindowPlayfab_OnRegisterButtonClicked_m7B7F8AEF31A7FB0031833B88DE532CA13BA97764 ();
// 0x00000065 System.Void LoginWindowPlayfab::.ctor()
extern void LoginWindowPlayfab__ctor_m6A8AC3BB1AFFEEF8E337F3805A48279BFB50F48B ();
// 0x00000066 System.Void LoginWindowPlayfab::<Awake>b__14_0(System.Boolean)
extern void LoginWindowPlayfab_U3CAwakeU3Eb__14_0_m014E5D48279B9F8FD7732280CDB2216AE753F127 ();
// 0x00000067 System.Void GameManagerMCQ::Start()
extern void GameManagerMCQ_Start_mC966015156DE497EBE8EF5ABA9A546C0D35421EB ();
// 0x00000068 System.Void GameManagerMCQ::Update()
extern void GameManagerMCQ_Update_m4691CF8DB892E0140534462F63BE0B0EB2726A32 ();
// 0x00000069 System.Void GameManagerMCQ::.ctor()
extern void GameManagerMCQ__ctor_mDF59B9F474F15E045877F356EA507C69ABF6ED31 ();
// 0x0000006A System.Void McqScript::Start()
extern void McqScript_Start_m5A4FFC8D5B5B5F5F861EAF73188F72A2794F7020 ();
// 0x0000006B System.Void McqScript::Update()
extern void McqScript_Update_m4E139366CACBD1A9F45CF844A595D4C11454D155 ();
// 0x0000006C System.Void McqScript::QuestionGen(System.Collections.Generic.List`1<QuestionsList>)
extern void McqScript_QuestionGen_mF6AA4F8F06C923309D6886AB7C3CC8E89AA15807 ();
// 0x0000006D System.Void McqScript::OptionA()
extern void McqScript_OptionA_m280D7BEFAED38F27B72D5E61283285407D30E406 ();
// 0x0000006E System.Void McqScript::OptionB()
extern void McqScript_OptionB_m66AE56A13FB70E4779015ABC5CC3AE3A680F5D1C ();
// 0x0000006F System.Void McqScript::OptionC()
extern void McqScript_OptionC_m6A5173F7D402AF5828EBEEC3FC39FEB0D77C0196 ();
// 0x00000070 System.Void McqScript::OptionD()
extern void McqScript_OptionD_mBE4BBB2715E5C17CC8AAF4C7A56AC048BF5D02DB ();
// 0x00000071 System.Void McqScript::CheckAnswer(System.String,System.Collections.Generic.List`1<QuestionsList>)
extern void McqScript_CheckAnswer_m67D03A43CE9BDD3C5C0E95579A6996189A3F1740 ();
// 0x00000072 System.Void McqScript::AddScore()
extern void McqScript_AddScore_mBCFB6E72A5367E5E23788987937AE3A55BA62853 ();
// 0x00000073 System.Void McqScript::NextButton()
extern void McqScript_NextButton_m15D4D4E4E26D6859F7BC3ABC27F980A2C2A970EA ();
// 0x00000074 System.Void McqScript::DisplayTime(System.Single)
extern void McqScript_DisplayTime_m0A78A19297D2905512B32FB21E83A6393B819CD3 ();
// 0x00000075 System.Collections.IEnumerator McqScript::Clock()
extern void McqScript_Clock_m489F50DBBA9671468C00842D7C6F082B428ED759 ();
// 0x00000076 System.Void McqScript::.ctor()
extern void McqScript__ctor_m208F9F1AD676AFC1201D4385B480428402C9E9D3 ();
// 0x00000077 System.Void McqScript_<Clock>d__30::.ctor(System.Int32)
extern void U3CClockU3Ed__30__ctor_mC0D277FA9444AFB6A1E04A226931F2F7FAABC420 ();
// 0x00000078 System.Void McqScript_<Clock>d__30::System.IDisposable.Dispose()
extern void U3CClockU3Ed__30_System_IDisposable_Dispose_m9CB07E9005614CBE6863AFF17B8C211138B55972 ();
// 0x00000079 System.Boolean McqScript_<Clock>d__30::MoveNext()
extern void U3CClockU3Ed__30_MoveNext_m488866622DCCC2520A5858A87765D03C711D4E6F ();
// 0x0000007A System.Object McqScript_<Clock>d__30::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CClockU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3F91DBB0F71DB765572390D468AD5A29A8D0571F ();
// 0x0000007B System.Void McqScript_<Clock>d__30::System.Collections.IEnumerator.Reset()
extern void U3CClockU3Ed__30_System_Collections_IEnumerator_Reset_mF0B6ECF0F17E741E32E4F085302003F01B5E0371 ();
// 0x0000007C System.Object McqScript_<Clock>d__30::System.Collections.IEnumerator.get_Current()
extern void U3CClockU3Ed__30_System_Collections_IEnumerator_get_Current_mE92CC83DFFE2705013D7E172A2E9DBEE8AC89D13 ();
// 0x0000007D System.Void QuestionsList::.ctor()
extern void QuestionsList__ctor_m05F257FA22DAB3A89727CEBB82CB8022868ED228 ();
// 0x0000007E System.Void BoardUI::MCQPanel(PlayFab.ClientModels.GetLeaderboardResult)
extern void BoardUI_MCQPanel_m83621F3AA60CA1970BCCE7DE377140B92A02179E ();
// 0x0000007F System.Void BoardUI::BoolPanel(PlayFab.ClientModels.GetLeaderboardResult)
extern void BoardUI_BoolPanel_m26D5F8AC8BB41A642AF8F1B16C9950CC587C1D84 ();
// 0x00000080 System.Void BoardUI::GetBoolLeaderboard()
extern void BoardUI_GetBoolLeaderboard_m8D236D92BA15EE8700EFFC4EEEEF9466F20E38B0 ();
// 0x00000081 System.Void BoardUI::GetMCQLeaderboard()
extern void BoardUI_GetMCQLeaderboard_m6F97CEA377B542AAFE9A4CF6BC068A0B2D1B485A ();
// 0x00000082 System.Void BoardUI::CloseLeaderboardPanel()
extern void BoardUI_CloseLeaderboardPanel_m74AA41E74F0B16F60D431A11F572D6D1DF36713C ();
// 0x00000083 System.Void BoardUI::OnErrorLeaderboard(PlayFab.PlayFabError)
extern void BoardUI_OnErrorLeaderboard_m037495207D625982B360BBE7DB84ACFD8581265A ();
// 0x00000084 System.Void BoardUI::.ctor()
extern void BoardUI__ctor_m9E3D88E30735B448400C918007FEAC31BDF4B3AC ();
// 0x00000085 System.Void LeaderList::.ctor()
extern void LeaderList__ctor_m9E597511947DD2253761B0D91475B190EC9BA316 ();
// 0x00000086 System.Void PlayfabWeb::GetStats()
extern void PlayfabWeb_GetStats_m766B5A52EA7E208B8BA47C7F2832A4E6E0F74AC0 ();
// 0x00000087 System.Void PlayfabWeb::OnGetStats(PlayFab.ClientModels.GetPlayerStatisticsResult)
extern void PlayfabWeb_OnGetStats_m1D1D2983446667A9EA88F35D256765CE507CA537 ();
// 0x00000088 System.Void PlayfabWeb::StartCloudUpdatePlayerStats()
extern void PlayfabWeb_StartCloudUpdatePlayerStats_m16DCBF6E4F73D3F056A8ED82761CA8C3B9B1CEAC ();
// 0x00000089 System.Void PlayfabWeb::OnCloudUpdatePlayerStats(PlayFab.ClientModels.ExecuteCloudScriptResult)
extern void PlayfabWeb_OnCloudUpdatePlayerStats_m2A6027223E8DFDE61C3C48564E60B3C28FC86CBA ();
// 0x0000008A System.Void PlayfabWeb::OnErrorShared(PlayFab.PlayFabError)
extern void PlayfabWeb_OnErrorShared_mE9B3ABF3964736D0CF6D65437302EDF1C59FDF6A ();
// 0x0000008B System.Void PlayfabWeb::.ctor()
extern void PlayfabWeb__ctor_m4B7155012296CC66139D53D7633ACE7AA8F18AF5 ();
// 0x0000008C System.Void PlayfabWeb_<>c::.cctor()
extern void U3CU3Ec__cctor_m1CBB3616A802D0A7E07F573CDE29C27E10C891EE ();
// 0x0000008D System.Void PlayfabWeb_<>c::.ctor()
extern void U3CU3Ec__ctor_mF40820A4FE84B762A36C1CD3EB51CD7578CE7819 ();
// 0x0000008E System.Void PlayfabWeb_<>c::<GetStats>b__3_0(PlayFab.PlayFabError)
extern void U3CU3Ec_U3CGetStatsU3Eb__3_0_mD30FF40831E589BC062E9564C2816453F65DA157 ();
// 0x0000008F System.Void ButtonsScript::Home()
extern void ButtonsScript_Home_mA08E38BBBD52463CAB14DCF799B1D1D67B6CB1CF ();
// 0x00000090 System.Void ButtonsScript::Play()
extern void ButtonsScript_Play_mDB84D06A3D0AD5C2C76D0AB8AD450FF405AF5E7B ();
// 0x00000091 System.Void ButtonsScript::LeaderBoard()
extern void ButtonsScript_LeaderBoard_mB6E79F760B7F2AF7B1380D34A9F927197043BB43 ();
// 0x00000092 System.Void ButtonsScript::Settings()
extern void ButtonsScript_Settings_mB71B9A74227251E086B3E1E73EB88E71B5682B8C ();
// 0x00000093 System.Void ButtonsScript::GuestRoomScenes()
extern void ButtonsScript_GuestRoomScenes_mE084D826B095B053CA8E3C787D2514AEE4346110 ();
// 0x00000094 System.Void ButtonsScript::DiningScenes()
extern void ButtonsScript_DiningScenes_m47F9C35BE21E8C52E8D67B87C863B80F5FB3DA81 ();
// 0x00000095 System.Void ButtonsScript::KitchenScenes()
extern void ButtonsScript_KitchenScenes_m67C4B04DDBC4FD69852B83309B5E569A36AFE827 ();
// 0x00000096 System.Void ButtonsScript::QuitPanel()
extern void ButtonsScript_QuitPanel_m65C10FB5E9CD7B1B150BAC7F89FD6ECF58840910 ();
// 0x00000097 System.Void ButtonsScript::QuitPanelOff()
extern void ButtonsScript_QuitPanelOff_m775998A80C58D08A33D4D31E3309D9844466574B ();
// 0x00000098 System.Void ButtonsScript::FinalQuit()
extern void ButtonsScript_FinalQuit_mE79C96DEB4A59CE5B5A3668673B5A7D5CD903E1D ();
// 0x00000099 System.Void ButtonsScript::ExittoRoom()
extern void ButtonsScript_ExittoRoom_mAA1638B3B069890AC6B0405D9F5872A6C8D43520 ();
// 0x0000009A System.Void ButtonsScript::.ctor()
extern void ButtonsScript__ctor_m308501930881AC6F95C61EACB5A29F08158F3EF3 ();
// 0x0000009B System.Void LoadingScript::Start()
extern void LoadingScript_Start_m1C30F6F4C9B2678BB6C5B8446781107ADAEB7307 ();
// 0x0000009C System.Void LoadingScript::Update()
extern void LoadingScript_Update_m4356B6DDF85A0D7817EF6DCD6BE688BE40AE4828 ();
// 0x0000009D System.Void LoadingScript::.ctor()
extern void LoadingScript__ctor_mD9953793E2AC471D3270517CDE8432DE3464AC1C ();
// 0x0000009E System.Void Settings::Awake()
extern void Settings_Awake_mBA445DDD366FCA51F1223575885A10ABDF812A21 ();
// 0x0000009F System.Void Settings::ChangeMusicToggle()
extern void Settings_ChangeMusicToggle_m4C5C2F1A4E5916AFF5AD29E3E01BBDE0FAD1590F ();
// 0x000000A0 System.Void Settings::ChangeSoundToggle()
extern void Settings_ChangeSoundToggle_mF6CBCAF85F457174DEA406AB726B45C79C8495C2 ();
// 0x000000A1 System.Void Settings::.ctor()
extern void Settings__ctor_m2E00E4F8E213532A3051E24E75CE65368C0BB661 ();
// 0x000000A2 System.Void VideoScript::Update()
extern void VideoScript_Update_mD4842166092C90050F96C0A014D6BF94055226C1 ();
// 0x000000A3 System.Void VideoScript::Skip()
extern void VideoScript_Skip_mFB205EEB3A9C38D6672C0CF663A8028832E1F7E9 ();
// 0x000000A4 System.Void VideoScript::.ctor()
extern void VideoScript__ctor_m7226CF609CC537EB050930B6AD05D783F854F547 ();
static Il2CppMethodPointer s_methodPointers[164] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BoolSumPlayer_Start_mCEB8ED6BB6B96328FA690D94F0BB2648D750DC7D,
	BoolSumPlayer_LateUpdate_m049F8403266B73BEF9C443C15DE8F624D782864C,
	BoolSumPlayer_Update_m2E6E222EF8D6D1B9CA420C685275FBBC8F616C2D,
	BoolSumPlayer_GetNewPlayer_m47EC900A444128A1711D2DF75393F535442132A5,
	BoolSumPlayer_Calculation_mCAE7FDEFF8B3A89746987D306D98A30F54C2D3A1,
	BoolSumPlayer_OnCollisionEnter2D_m39C632819970C8FA198158B317BF620526CD14BC,
	BoolSumPlayer__ctor_mE2ECB0C2FB1A27C8B9C462AF87DB603CF171676E,
	CameraControl_Start_m77CCD413451A457E13B0F9D4EBB0D76633CEC5C1,
	CameraControl_Update_m4BD3B811296B1A0E5C8FF3148DC11622A6430F6F,
	CameraControl_DeleteBoard_mE895591B51C9038849261B1086F88207DF8D0082,
	CameraControl__ctor_m67DAD57065E2F985D5F050A4DDD3DB9B95548100,
	U3CDeleteBoardU3Ed__7__ctor_mBE59F1CB4F1E20C7B6438FF842C58540FA559673,
	U3CDeleteBoardU3Ed__7_System_IDisposable_Dispose_m37213975F4ACA7036620FA1830FE4CA26849D3D5,
	U3CDeleteBoardU3Ed__7_MoveNext_mF79C8FCEEEAC8963EC510A17389E9B2C2AFBB345,
	U3CDeleteBoardU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m630FEAC85049F2A8BDEE2F6C3D14C6AA187C454C,
	U3CDeleteBoardU3Ed__7_System_Collections_IEnumerator_Reset_m49D517EAAEF0087714E9AD0F57A53C5F3E0458A6,
	U3CDeleteBoardU3Ed__7_System_Collections_IEnumerator_get_Current_mE17AAA7B2A7796ED2134AA80EC68F13F366BFDC1,
	GameManager_Start_m26461AEF27E44DB8FECCBC19D6C9E228B658BF8E,
	GameManager_Update_mC9303BA7C3117BD861F49F8E36151CC52117E6C1,
	GameManager_DisplayTime_m7DEC035DE877FFD5EA6BD7EF33593CEF83A25475,
	GameManager_CompleteMission_m25F8091EEABBC1F93B6DED280C2067355D1A7217,
	GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D,
	MissionsScript_MissionRandomiser_m0022AC1AF800608888E77E9B0B6B8BACB6BDC37E,
	MissionsScript__ctor_mC72E74999EEE1FDBA20FCC365BDE927DD520BF8B,
	MovingCollider_Start_mB4BC3EA0F3410BFCF31D3A90F50ADC166DC57718,
	MovingCollider_Update_m828EDD50635EA4A13D62B81B9B9C72E9A708FCCB,
	MovingCollider__ctor_m5C790BF66AA7688A8E4D8D97BD660AE86F70BFDD,
	ObjectSpawnerScript_BaseSpawning_mB8ED61311424EA26F8286BDDF9212188E84DF4B6,
	ObjectSpawnerScript_BaseWaveStart_m9C7C4969CEABBE5E005D43240245D5027BFD8D22,
	ObjectSpawnerScript_DeleteSpawns_m3C962DF2F24E6F05BC4F7C522062C31AB9755AD5,
	ObjectSpawnerScript_Start_mA07578CBC153796A1DFF1E10658C006E46465BE3,
	ObjectSpawnerScript_Update_mD89DC6EA967F2B51A5D2F7EB98D78252B7DE4F1B,
	ObjectSpawnerScript__ctor_m03D588905A1E88882F6F1E22D9CCD34E3E9C95B7,
	SpawnRate__ctor_mFF26BE9EBFE3DFD14FD12007A7AAFCD586BBC1B6,
	TouchScreen_Update_m20C1CF1312B2FC6A3E52947B41EE3B27CB89C1FE,
	TouchScreen__ctor_m3DB45801B2C8CC442B1DFEAF8F0A75B1545B973E,
	AuthService_add_OnDisplayAuthentication_mB030003EB4E0A840A5ECFA4EC9911EB4AC80AFA7,
	AuthService_remove_OnDisplayAuthentication_mCE1875B9CEF3DA374196DDB63C9E791623489606,
	AuthService_add_OnLoginSuccess_m7CB60D72C0F18B5696E53DBD1724CD56C1D77B2D,
	AuthService_remove_OnLoginSuccess_mD65EA9217985E6C9656DE8D00AEB20E6C4A922ED,
	AuthService_add_OnPlayFabError_mFC753F219C54BEFA34EAF87E1249DA8DA20A7C78,
	AuthService_remove_OnPlayFabError_m14A39214E13C2B0AEF50228F40884EFCAC3A428D,
	AuthService_get_PlayFabId_m64252895061CC0311B4E900EED7B9FECCF9E160C,
	AuthService_get_SessionTicket_m0EC3EB5DB640E30A3C41D61A2F3A8B722C511574,
	AuthService_get_Instance_m3FB7F648BD0BC4E1799DBE249F553053D055114D,
	AuthService__ctor_m0623CA575881E4EA27E6EBB5AD6272D0A91645A0,
	AuthService_get_RememberMe_mCC09C5C31DA042632C9358C6583B9D3F73A9B758,
	AuthService_set_RememberMe_m9A656CAA8ECEBF780E5EC64F2D5A35031B8EF8FA,
	AuthService_get_AuthType_mDC2915DB955E2A41ACC2D067EFD42E1C7DCA5DCF,
	AuthService_set_AuthType_m845215E4A772203B4FD23A6A944A61D35617D6BB,
	AuthService_get_RememberMeId_m572AB71FC8D518FB0B29C947175564FD81A2E548,
	AuthService_set_RememberMeId_m057CD235046D5A5506FD628AE75CB7EA5C2D8899,
	AuthService_ClearRememberMe_mC36914419ACBBB6DAEFF142D34137D18DDD7E8A0,
	AuthService_Authenticate_mDB8E51592BE3841BCF347D5E8A626ABDD00FD2AE,
	AuthService_Authenticate_mF215D4E5E616A7FF9C34997377A5A046F85C72B9,
	AuthService_AuthenticateEmailPassword_mCB6A295C25A1B858DFF867BA97039C592E7AB3F4,
	AuthService_AddAccountAndPassword_mFC617E53AB774910FB11E035F46354BC14C1B1AE,
	AuthService_SilentlyAuthenticate_m4DE0BDC374255862B7815E9EAC8E56E62855DE18,
	AuthService_UnlinkSilentAuth_m02CBA8730DBB4AF98A946606845B914301A5E7AD,
	AuthService_U3CAuthenticateEmailPasswordU3Eb__43_2_mCB05DE9FF60C6A0A5AC52B443C205B641DEDF0CA,
	AuthService_U3CAddAccountAndPasswordU3Eb__44_0_m88FE57B28A7397CF8E102328C204E236E1C09E3D,
	DisplayAuthenticationEvent__ctor_mFE2A0C7477EC532A3BF5D959173D01201994A817,
	DisplayAuthenticationEvent_Invoke_m43469A76922AA592F0257615E4FE6E1DECAF0DB6,
	DisplayAuthenticationEvent_BeginInvoke_m682896BED0B790EE97E3C82C0F49F2997B3B7A7A,
	DisplayAuthenticationEvent_EndInvoke_m5C68F299EFD1C9509E13ABD79758F1972A4A4302,
	LoginSuccessEvent__ctor_m8C0E4C223A07DE08F432F587611D2D594B5877A6,
	LoginSuccessEvent_Invoke_m928B62C46B7CE5AF3242BA6CB1BA84E291F091A4,
	LoginSuccessEvent_BeginInvoke_m9D2EED28830762BD55C4564B1AA652CD115A36D1,
	LoginSuccessEvent_EndInvoke_m540A997912D92B52DBA50C3997FF7752C5ADE61B,
	PlayFabErrorEvent__ctor_m8AAE0396F2E88C9A50BE86FA307B60C235E4209C,
	PlayFabErrorEvent_Invoke_mE6E7170DB000147693299FDB334AE2CE2D6F203B,
	PlayFabErrorEvent_BeginInvoke_m693F00F683A07C4554137803F310E310FAC8A3BA,
	PlayFabErrorEvent_EndInvoke_m40E31113991BF65DBDDF30FA87719980A7FE2D24,
	U3CU3Ec__cctor_mE4E23231CC95C6602664DA4AF7EEC0C4377BC8EC,
	U3CU3Ec__ctor_m8CA9F89CE5483E899543F5AD191A68966FD4C2D5,
	U3CU3Ec_U3CAuthenticateEmailPasswordU3Eb__43_0_mB783A9A5F580BE386CB8D942795ADCE63D8B3101,
	U3CU3Ec_U3CAuthenticateEmailPasswordU3Eb__43_1_m430BED5854C55C79341EBC51584B447B6228BC11,
	U3CU3Ec_U3CAuthenticateEmailPasswordU3Eb__43_3_mB1D530369E773A809663B7648261B724E2A4FB4A,
	U3CU3Ec_U3CAddAccountAndPasswordU3Eb__44_2_mD20B916F7B2B711359753C2C1232021F8DB970F8,
	U3CU3Ec_U3CUnlinkSilentAuthU3Eb__46_0_mB9D2B53EC27797CD8F37D1500869D37821CB2794,
	U3CU3Ec__DisplayClass44_0__ctor_mF6DD4D1975C17D289BB32AF01F902B8FCED156E3,
	U3CU3Ec__DisplayClass44_0_U3CAddAccountAndPasswordU3Eb__1_m075298722917038E3AA4446368E8A34D9611B3ED,
	U3CU3Ec__DisplayClass45_0__ctor_mAEC48DD405E3E2CEAD65C6CBF74BB7EA2BC49549,
	U3CU3Ec__DisplayClass45_0_U3CSilentlyAuthenticateU3Eb__0_mCCFA24C6153D7244619FE00F7C41D52C29E71D10,
	U3CU3Ec__DisplayClass45_0_U3CSilentlyAuthenticateU3Eb__1_m4B2A81C9B2481F6FD433B928EEFDA13A885C68E4,
	LoginWindowPlayfab_Awake_m80FF93415DFB578A3EFD38B97F4F7E7C85460847,
	LoginWindowPlayfab_Start_mB6D25C0B9FB8CFA1AF9A1FEE06FC96A3A9C718D3,
	LoginWindowPlayfab_OnLoginSuccess_m5F2F6603E63FAA895C4B65159DD8077B2F021BFF,
	LoginWindowPlayfab_OnDisplayName_mBDD55EBC6D85E6DE137401299FF2492DB18D6D8A,
	LoginWindowPlayfab_OnPlayFaberror_mA8E385ACFAFB22A577E656C695DF198906EE3022,
	LoginWindowPlayfab_OnDisplayAuthentication_m82D4BB0D9578E5F3A4C12F4B6CC819433945A94B,
	LoginWindowPlayfab_OnPlayAsGuestClicked_mADF839C900EB8D9A4AB984C7A05E590288152527,
	LoginWindowPlayfab_OnLoginClicked_m5625731F3EBDEAD529F237AEA999B849967F5508,
	LoginWindowPlayfab_OnRegisterButtonClicked_m7B7F8AEF31A7FB0031833B88DE532CA13BA97764,
	LoginWindowPlayfab__ctor_m6A8AC3BB1AFFEEF8E337F3805A48279BFB50F48B,
	LoginWindowPlayfab_U3CAwakeU3Eb__14_0_m014E5D48279B9F8FD7732280CDB2216AE753F127,
	GameManagerMCQ_Start_mC966015156DE497EBE8EF5ABA9A546C0D35421EB,
	GameManagerMCQ_Update_m4691CF8DB892E0140534462F63BE0B0EB2726A32,
	GameManagerMCQ__ctor_mDF59B9F474F15E045877F356EA507C69ABF6ED31,
	McqScript_Start_m5A4FFC8D5B5B5F5F861EAF73188F72A2794F7020,
	McqScript_Update_m4E139366CACBD1A9F45CF844A595D4C11454D155,
	McqScript_QuestionGen_mF6AA4F8F06C923309D6886AB7C3CC8E89AA15807,
	McqScript_OptionA_m280D7BEFAED38F27B72D5E61283285407D30E406,
	McqScript_OptionB_m66AE56A13FB70E4779015ABC5CC3AE3A680F5D1C,
	McqScript_OptionC_m6A5173F7D402AF5828EBEEC3FC39FEB0D77C0196,
	McqScript_OptionD_mBE4BBB2715E5C17CC8AAF4C7A56AC048BF5D02DB,
	McqScript_CheckAnswer_m67D03A43CE9BDD3C5C0E95579A6996189A3F1740,
	McqScript_AddScore_mBCFB6E72A5367E5E23788987937AE3A55BA62853,
	McqScript_NextButton_m15D4D4E4E26D6859F7BC3ABC27F980A2C2A970EA,
	McqScript_DisplayTime_m0A78A19297D2905512B32FB21E83A6393B819CD3,
	McqScript_Clock_m489F50DBBA9671468C00842D7C6F082B428ED759,
	McqScript__ctor_m208F9F1AD676AFC1201D4385B480428402C9E9D3,
	U3CClockU3Ed__30__ctor_mC0D277FA9444AFB6A1E04A226931F2F7FAABC420,
	U3CClockU3Ed__30_System_IDisposable_Dispose_m9CB07E9005614CBE6863AFF17B8C211138B55972,
	U3CClockU3Ed__30_MoveNext_m488866622DCCC2520A5858A87765D03C711D4E6F,
	U3CClockU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3F91DBB0F71DB765572390D468AD5A29A8D0571F,
	U3CClockU3Ed__30_System_Collections_IEnumerator_Reset_mF0B6ECF0F17E741E32E4F085302003F01B5E0371,
	U3CClockU3Ed__30_System_Collections_IEnumerator_get_Current_mE92CC83DFFE2705013D7E172A2E9DBEE8AC89D13,
	QuestionsList__ctor_m05F257FA22DAB3A89727CEBB82CB8022868ED228,
	BoardUI_MCQPanel_m83621F3AA60CA1970BCCE7DE377140B92A02179E,
	BoardUI_BoolPanel_m26D5F8AC8BB41A642AF8F1B16C9950CC587C1D84,
	BoardUI_GetBoolLeaderboard_m8D236D92BA15EE8700EFFC4EEEEF9466F20E38B0,
	BoardUI_GetMCQLeaderboard_m6F97CEA377B542AAFE9A4CF6BC068A0B2D1B485A,
	BoardUI_CloseLeaderboardPanel_m74AA41E74F0B16F60D431A11F572D6D1DF36713C,
	BoardUI_OnErrorLeaderboard_m037495207D625982B360BBE7DB84ACFD8581265A,
	BoardUI__ctor_m9E3D88E30735B448400C918007FEAC31BDF4B3AC,
	LeaderList__ctor_m9E597511947DD2253761B0D91475B190EC9BA316,
	PlayfabWeb_GetStats_m766B5A52EA7E208B8BA47C7F2832A4E6E0F74AC0,
	PlayfabWeb_OnGetStats_m1D1D2983446667A9EA88F35D256765CE507CA537,
	PlayfabWeb_StartCloudUpdatePlayerStats_m16DCBF6E4F73D3F056A8ED82761CA8C3B9B1CEAC,
	PlayfabWeb_OnCloudUpdatePlayerStats_m2A6027223E8DFDE61C3C48564E60B3C28FC86CBA,
	PlayfabWeb_OnErrorShared_mE9B3ABF3964736D0CF6D65437302EDF1C59FDF6A,
	PlayfabWeb__ctor_m4B7155012296CC66139D53D7633ACE7AA8F18AF5,
	U3CU3Ec__cctor_m1CBB3616A802D0A7E07F573CDE29C27E10C891EE,
	U3CU3Ec__ctor_mF40820A4FE84B762A36C1CD3EB51CD7578CE7819,
	U3CU3Ec_U3CGetStatsU3Eb__3_0_mD30FF40831E589BC062E9564C2816453F65DA157,
	ButtonsScript_Home_mA08E38BBBD52463CAB14DCF799B1D1D67B6CB1CF,
	ButtonsScript_Play_mDB84D06A3D0AD5C2C76D0AB8AD450FF405AF5E7B,
	ButtonsScript_LeaderBoard_mB6E79F760B7F2AF7B1380D34A9F927197043BB43,
	ButtonsScript_Settings_mB71B9A74227251E086B3E1E73EB88E71B5682B8C,
	ButtonsScript_GuestRoomScenes_mE084D826B095B053CA8E3C787D2514AEE4346110,
	ButtonsScript_DiningScenes_m47F9C35BE21E8C52E8D67B87C863B80F5FB3DA81,
	ButtonsScript_KitchenScenes_m67C4B04DDBC4FD69852B83309B5E569A36AFE827,
	ButtonsScript_QuitPanel_m65C10FB5E9CD7B1B150BAC7F89FD6ECF58840910,
	ButtonsScript_QuitPanelOff_m775998A80C58D08A33D4D31E3309D9844466574B,
	ButtonsScript_FinalQuit_mE79C96DEB4A59CE5B5A3668673B5A7D5CD903E1D,
	ButtonsScript_ExittoRoom_mAA1638B3B069890AC6B0405D9F5872A6C8D43520,
	ButtonsScript__ctor_m308501930881AC6F95C61EACB5A29F08158F3EF3,
	LoadingScript_Start_m1C30F6F4C9B2678BB6C5B8446781107ADAEB7307,
	LoadingScript_Update_m4356B6DDF85A0D7817EF6DCD6BE688BE40AE4828,
	LoadingScript__ctor_mD9953793E2AC471D3270517CDE8432DE3464AC1C,
	Settings_Awake_mBA445DDD366FCA51F1223575885A10ABDF812A21,
	Settings_ChangeMusicToggle_m4C5C2F1A4E5916AFF5AD29E3E01BBDE0FAD1590F,
	Settings_ChangeSoundToggle_mF6CBCAF85F457174DEA406AB726B45C79C8495C2,
	Settings__ctor_m2E00E4F8E213532A3051E24E75CE65368C0BB661,
	VideoScript_Update_mD4842166092C90050F96C0A014D6BF94055226C1,
	VideoScript_Skip_mFB205EEB3A9C38D6672C0CF663A8028832E1F7E9,
	VideoScript__ctor_m7226CF609CC537EB050930B6AD05D783F854F547,
};
static const int32_t s_InvokerIndices[164] = 
{
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	23,
	23,
	28,
	27,
	26,
	23,
	23,
	23,
	14,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	341,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	1476,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	159,
	159,
	159,
	159,
	159,
	159,
	4,
	4,
	4,
	23,
	89,
	31,
	10,
	32,
	14,
	26,
	23,
	32,
	23,
	23,
	23,
	26,
	23,
	26,
	26,
	131,
	23,
	105,
	26,
	131,
	26,
	211,
	26,
	131,
	26,
	211,
	26,
	3,
	23,
	26,
	26,
	26,
	26,
	26,
	23,
	26,
	23,
	26,
	26,
	23,
	23,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	27,
	23,
	23,
	341,
	14,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	26,
	26,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	26,
	23,
	159,
	159,
	23,
	3,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x02000002, { 0, 11 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[11] = 
{
	{ (Il2CppRGCTXDataType)2, 15961 },
	{ (Il2CppRGCTXDataType)3, 10625 },
	{ (Il2CppRGCTXDataType)2, 15962 },
	{ (Il2CppRGCTXDataType)3, 10626 },
	{ (Il2CppRGCTXDataType)3, 10627 },
	{ (Il2CppRGCTXDataType)2, 15963 },
	{ (Il2CppRGCTXDataType)3, 10628 },
	{ (Il2CppRGCTXDataType)3, 10629 },
	{ (Il2CppRGCTXDataType)3, 10630 },
	{ (Il2CppRGCTXDataType)2, 15544 },
	{ (Il2CppRGCTXDataType)2, 15545 },
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	164,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	11,
	s_rgctxValues,
	NULL,
};
